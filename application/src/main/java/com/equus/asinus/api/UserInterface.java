package com.equus.asinus.api;

import com.equus.asinus.domain.models.User;

import java.util.List;
import java.util.Optional;

public interface UserInterface {
    void addUser(User user) throws Exception;

    void deleteUser(User user);

    List<User> getUsers();

    User getUserById(String id);

    Optional<User> findByUsername(String username);
}
