package com.equus.asinus.config;

import com.equus.asinus.adapter.UserServiceAdapter;
import com.equus.asinus.api.UserInterface;
import com.equus.asinus.domain.interfaces.port.server.UserPersistencePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfiguration {

    @Bean
    public UserInterface getUserService(UserPersistencePort userPersistencePort) {
        return new UserServiceAdapter(userPersistencePort);
    }
}
