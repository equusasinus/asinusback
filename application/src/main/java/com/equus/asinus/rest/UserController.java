package com.equus.asinus.rest;

import com.equus.asinus.adapter.UserServiceAdapter;
import com.equus.asinus.domain.models.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/users")
@Slf4j
public class UserController {
    @Autowired
    private UserServiceAdapter userDetailsService;

    @PostMapping(value = "/users")
    public ResponseEntity<List<User>> findAll()  {
        List<User> users = userDetailsService.getUsers();
        users.forEach(System.out::println);
        return ResponseEntity.ok(users);
    }
}
