package com.equus.asinus.security.config;

import java.util.*;
import java.util.stream.Collectors;

import com.equus.asinus.domain.models.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Data
@NoArgsConstructor
@ToString
public class UserDetailsImpl implements UserDetails {
    private static final long                                   serialVersionUID = 1L;
    private              String                                 userId;
    private              String                                 firstName;
    private              String                                 username;
    private              String                                 password;
    private              String                                 lastName;
    private              Collection<? extends GrantedAuthority> authorities;
    private              boolean                                accountNonExpired;
    private              boolean                                accountNonLocked;
    private              boolean                                credentialsNonExpired;
    private              boolean                                enabled;

    public UserDetailsImpl(User user) {
        this.userId = user.getUserId();
        this.firstName = user.getFirstName();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.lastName = user.getLastName();
        this.authorities = user.getAuthorities().stream()
                               .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                               .collect(Collectors.toList());
        ;
        this.accountNonExpired = user.isAccountNonExpired();
        this.accountNonLocked = user.isAccountNonLocked();
        this.credentialsNonExpired = user.isCredentialsNonExpired();
        this.enabled = user.isEnabled();
    }

    public static UserDetailsImpl build(User user) {
        return new UserDetailsImpl(user);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(userId, user.getUserId());
    }

}
