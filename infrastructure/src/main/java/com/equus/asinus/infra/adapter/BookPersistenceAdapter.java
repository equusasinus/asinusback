package com.equus.asinus.infra.adapter;

import com.equus.asinus.domain.interfaces.port.server.BookPersistencePort;
import com.equus.asinus.domain.models.Book;

import java.util.List;

public class BookPersistenceAdapter implements BookPersistencePort {

    @Override
    public void addBook(Book book) {

    }

    @Override
    public void deleteBook(Book book) {

    }

    @Override
    public List<Book> getBooks() {
        return null;
    }

    @Override
    public Book getBookById(String id) {
        return null;
    }
}
