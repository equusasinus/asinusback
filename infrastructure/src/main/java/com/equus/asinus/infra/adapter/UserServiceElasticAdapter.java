package com.equus.asinus.infra.adapter;

import com.equus.asinus.domain.interfaces.port.server.UserPersistencePort;
import com.equus.asinus.domain.models.User;
import com.equus.asinus.infra.entity.UserEntity;
import com.equus.asinus.infra.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
public class UserServiceElasticAdapter implements UserPersistencePort{
    private UserRepository userRepository;

    public UserServiceElasticAdapter(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(User user) throws Exception{
        UserEntity entity = new UserEntity();
        BeanUtils.copyProperties(user,entity);
        userRepository.save(entity);
    }

    @Override
    public void deleteUser(User user) {

    }

    @Override
    public List<User> getUsers() {
        Iterable<UserEntity> userEntities = userRepository.findAll();
        return StreamSupport.stream(userEntities.spliterator(), false)
                                           .map(e->{
                                                    User user = new User();
                                                    BeanUtils.copyProperties(e,user);
                                                    return user;
                                                })
                                           .collect(Collectors.toList());
    }

    @Override
    public User getUserById(String id) {
        return null;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        try{
            Optional<UserEntity> entityOptional = userRepository.findByUsername(username);
            if (entityOptional.isPresent()){
                UserEntity entity = entityOptional.get();
                User user = new User();
                BeanUtils.copyProperties(entity,user);
                return Optional.of(user);
            }
        }catch(Exception e){
            if (log.isDebugEnabled()){
                log.error("Error "+e.getMessage());
            }
        }
        return Optional.empty();
    }
}
