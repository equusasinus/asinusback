package com.equus.asinus.infra.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "bookdb_index")
public class BookEntity {
    @Id
    private String id;
    private String author;
    private String name;
}
