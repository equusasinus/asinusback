package com.equus.asinus.infra.entity;

import com.equus.asinus.domain.models.ERole;
import com.equus.asinus.domain.models.Role;
import lombok.*;
import org.elasticsearch.common.Nullable;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "userdb_index")
public class UserEntity {
    @Id
    private String    userId;
    private String    firstName;
    private String    lastName;
    private String    password;
    private String    username;
    @Nullable
    @Field(name = "birthday", type = FieldType.Date, format = DateFormat.basic_date)
    private LocalDate birthday;
    private String     email;
    private Set<Role> authorities = new HashSet<>();
    private boolean    accountNonExpired;
    private boolean   accountNonLocked;
    private boolean   credentialsNonExpired;
    private boolean   enabled;

}
