package com.equus.asinus.infra.repository;

import java.util.List;


import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.equus.asinus.domain.models.Book;

@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String> {

}
