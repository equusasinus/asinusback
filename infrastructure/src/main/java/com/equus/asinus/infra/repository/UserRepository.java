package com.equus.asinus.infra.repository;

import com.equus.asinus.infra.entity.UserEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserRepository extends ElasticsearchRepository<UserEntity,String> {

    Optional<UserEntity> findByUsername(String username) throws Exception;

}
