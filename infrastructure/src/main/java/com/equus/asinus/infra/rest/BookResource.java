package com.equus.asinus.infra.rest;

import com.equus.asinus.domain.models.Book;
import com.equus.asinus.infra.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/book")
@Slf4j
public class BookResource {
    @Autowired
    private BookService bookService;

    @PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> insertBook(@RequestBody Book book) {
        book.setId(UUID.randomUUID().toString());
        try{
            bookService.addBook(book);
            if (log.isDebugEnabled()){
                log.info("Object saved {}",book.toString());
            }
            return ResponseEntity.ok(Map.of("id", book.getId()));
        }catch(Exception e){
            if (log.isDebugEnabled()){
                log.error("Book not inserted {}",book.toString());
            }
            return ResponseEntity.ok(Map.of("error", "Book not inserted"));
        }
    }
}
